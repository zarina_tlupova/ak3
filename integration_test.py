# pylint: disable=missing-class-docstring, disable=useless-import-alias, disable=missing-function-docstring 
# pylint: disable=unused-import, disable=trailing-whitespace, disable=missing-module-docstring
# pylint: disable=trailing-whitespace
import contextlib
import io
import os
import tempfile
import unittest
import logging
import pytest as pytest

import translator
import machine


@pytest.mark.golden_test("examples/*.yml")
def test_whole_by_golden(golden, caplog):

    # Установим уровень отладочного вывода на DEBUG
    caplog.set_level(logging.DEBUG)

    # Создаём временную папку для тестирования приложения.
    with tempfile.TemporaryDirectory() as tmpdirname:
        # Готовим имена файлов для входных и выходных данных.
        source_file = os.path.join(tmpdirname, "temp.lisp")
        # source_file = factory.get("source")
        target = os.path.join(tmpdirname, "temp.json")
        input_file = os.path.join(tmpdirname, "input.txt")

        # Записываем входные данные в файлы. Данные берутся из теста.
        with open(source_file, "w", encoding="utf-8") as file:
            file.write(golden["source"])
        with open(input_file, "w", encoding="utf-8") as file:
            file.write(golden["input"])

        # Запускаем транслятор и собираем весь стандартный вывод в переменную stdout
        with contextlib.redirect_stdout(io.StringIO()) as stdout:
            translator.main([source_file, target])
            machine.main([target, input_file])

        # Выходные данные также считываем в переменные.
        with open(target, encoding="utf-8") as file:
            code = file.read()

        # Проверяем, что ожидания соответствуют реальности.
        assert code == golden.out["code"]
        assert stdout.getvalue() == golden.out["output"]
        assert caplog.text == golden.out["log"]
